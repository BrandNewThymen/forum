<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use AppBundle\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CategoryController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */

    public function index()
    {

        /** @var CategoryRepository $categories */
        $categories = $this->getDoctrine()
            ->getRepository(Category::class);

        // Get all categories
        $categories = $categories->findAll();

        // Create empty array to parse data into
        $temp = [];

        foreach($categories as $category) {
            // Asign values to the empty array
            $temp[] = [
                "id" => $category->getId(),
                "title" => $category->getTitle(),
                "description" => $category->getDescription(),
                "parentId" => $category->getParentId()
            ];
        }

        $buildTree = function(array &$temp, $parentId=0) use(&$buildTree) {

            // Create empty array to put data into
            $branch = [];

            // Foreach temp as val
            foreach($temp as $val) {

                // if parentId matches id add to children $val['children'][] = $val;
                if ($val["parentId"] == $parentId){
                    // Set children
                    $children = $buildTree($temp, $val["id"]);

                    // If children exist
                    if (!empty($children)) {

                        // Assign children
                        $val["children"] = $children;

                    }

                    $branch[$val["id"]] = $val;

                    unset($temp[$val["id"]]);
                }

                // if not then append normally with EMPTY children

                // Pass data to $buildTree and compare again!
            }
            return $branch;
        };

        // Executing structuring code
        $categories = $buildTree($temp);

        function recursive($array, $level = 0)
        {
            foreach($array as $category)
            {
                echo str_repeat("&emsp;", $level), "<ul>", '';
                echo "<a href=http://localhost/forum/web/app_dev.php/showtopics/". $category["id"]. ">";
//                echo "<a href=" . $this->generateUrl("show_topics", array('slug' => $category["id"])) . ">";
                echo $category["title"];
                echo "</a>";
                if(isset($category['children'])) {
                    echo '<br/>';
                    recursive($category['children'], $level + 1);
                    echo str_repeat("&emsp;", $level);
                }
                echo "</ul>";

            }
        }

        $categories = recursive($categories);


//        return new Response($categories);

        return $this->render('base.html.twig', [
            'categories' => $categories
        ]);
    }

    /**
     * @Route("showtopics/{categoryId}", name="show_topics")
     */
    public function showTopicsAction($categoryId)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($categoryId);

        // Get all topics with the category ID
        $topics = $category->getTopics();

        return $this->render('category/category.html.twig', [
            'category' => $category,
            'topics' => $topics
        ]);
    }
}