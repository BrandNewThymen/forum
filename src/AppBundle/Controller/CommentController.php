<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Repository\CategoryRepository;
use Doctrine\DBAL\Types\TextType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    /**
     * @Route(addcomment} name="add_comment")
     */
    public function addComment(Request $request)
    {

        $comment = new Comment();
        $comment->setContent('this is the content of our post!');
        $comment->setUser();
        $comment->setPost();

        $form = $this->createFormBuilder($comment)
            ->add('content', TextType::class)
            ->add('save', SubmitType::class, array('label' => 'Create comment'))
            ->getForm();


        return $this->render('comment.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}