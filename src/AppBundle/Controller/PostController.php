<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;

class PostController extends Controller
{
    /**
     * @Route("showpost/{postId}", name="show_post")
     */
    public function showAction($postId)
    {
//        $repository = $this->getDoctrine()
//            ->getRepository(Post::class);
//
//        $query = $repository->createQueryBuilder('p')
//            ->where('p.id = ?1')
//            ->setParameter(1, $postId)
//            ->getQuery();
//        $post = $query->getSingleResult();
//
//        $repository = $this->getDoctrine()
//            ->getRepository(Comment::class);
//
//        $query = $repository->createQueryBuilder('c')
//            ->where('c.post =?1')
//            ->setParameter(1, $postId)
//            ->getQuery();
//        $comments = $query->getResult();


        //SELECT user.username, comment.content FROM comment INNER JOIN user ON user.id = comment.user_id
        //SELECT post.content, user.username, post.id FROM post INNER JOIN user ON post.user_id = user.id and post.id = $postId

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT u.username, c.content
            FROM AppBundle:Comment c
            INNER JOIN AppBundle:User u
            WITH u.id = c.user
            AND c.post = (:postId)'
        )->setParameters(array(
            'postId' => $postId
        ));
        $comments = $query->getResult();

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p.content, u.username, p.id
            FROM AppBundle:Post p
            INNER JOIN AppBundle:User u
            WITH p.user = u.id
            AND p.id = (:postId)'
        )->setParameters(array(
            'postId' => $postId
        ));
        $post = $query->getSingleResult();

        return $this->render('post/post.html.twig', [
            'post' => $post,
            'comments' => $comments
        ]);
    }
}