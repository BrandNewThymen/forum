<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use AppBundle\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class TopicController
 * @package AppBundle\Controller
 * @Route("/topic")
 */

class TopicController extends Controller
{
    /**
     * @Route("/topicindex", name="")
     */
    public function showAllTopicsAction(){

        $topics = $this->getDoctrine()
            ->getRepository(Topic::class);

        // Get all topics
        $topics = $topics->findAll();

        // Create empty array to parse data into
        $temp = [];

        foreach($topics as $topic) {
            // Asign values to the empty array
            $temp[] = [
                "id" => $topic->getId(),
                "title" => $topic->getTitle(),
                "description" => $topic->getDescription(),
                "category" => $topic->getCategory()
            ];
        }


        foreach($temp as $topic) {
            echo $topic["id"];
            echo $topic["title"];
            echo $topic["description"]. "<br />";
        }

        return new response($temp);
    }

    /**
     * @Route("showtopic/{topicId}", name="show_topic")
     */
    public function showAction($topicId)
    {
        $topic = $this->getDoctrine()
            ->getRepository(Topic::class)
            ->find($topicId);

        $posts = $topic->getPosts();

        return $this->render('topic/topic.html.twig', [
            'topic' => $topic,
            'posts' => $posts
        ]);
    }


//    public function addCommentAction(Request $request, $id)
//    {
//        $add_comment = new Comment();
//        $add_comment->setTopicId($id);
//
//        $form = $this->createFormBuilder($add_comment)
//            ->add('topic_id', 'hidden')
//            ->add('author', 'text')
//            ->add('comment', 'textarea')
//            ->getForm();
//
//        if ($request->getMethod() == 'POST') {
//            $form->bindRequest($request);
//
//            if ($form->isValid()) {
//                $comment = $form->getData();
//                $em = $this->getDoctrine()->getEntityManager();
//                $em->persist($comment);
//                $em->flush();
//
//                return $this->redirect($this->generateUrl('/'));
//            }
//        }
//
//        return $this->render('AppBundle:Topic:add_comment.html.twig', array('form' => $form->createView(), 'id' => $id));
//    }
}