<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\Topic;
use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use AppBundle\Repository\CategoryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    /**
     * @Route("showuser/{userId}", name="show_user")
     */
    public function showUser($userId)
    {
        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->find($userId);

        dump($user);

        return new response('test');
    }

//    /**
//     * @Route("showposts/{postId}", name="show_post")
//     */
//    public function showPost($postId)
//    {
//        $post = $this->getDoctrine()
//            ->getRepository(Post::class)
//            ->find($postId);
//
//        $users = $post->getUser();
//
//        dump($users);
//
//        return new response('alweer een test');
//    }

}